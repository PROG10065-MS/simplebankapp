﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BankingAppLogic;

namespace SimpleBankApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //TODO: define a field variable of type Bank
        private Bank _bank;

        public MainPage()
        {
            this.InitializeComponent();

            //create the bank
            _bank = new Bank();
        }

        //TODO: define an event handler invoked when the Create Account button
        //is clicked. Implement the event handler that obtains the account data and
        //create an account object. Add the account to the bank and display the account
        //in the list of accounts (_lstAccountList) by adding it to its Items collection
        private void OnCreateAccount(object sender, RoutedEventArgs e)
        {
            //obtain the account data from the user via UI controls
            int acctNo = int.Parse(_txtAcctNo.Text);
            string acctHolderName = _txtAcctHolder.Text;
            AccountType acctType = (AccountType)(_cmbAcctType.SelectedIndex + 1);
            float annualIntrRate = float.Parse(_txtIntrRate.Text);
            double balance = double.Parse(_txtInitBalance.Text);

            //open the account with the bank object and set the properties of the account
            //to the required values provided by the user
            Account acct = _bank.OpenAccount(acctHolderName, acctType, acctNo);
            acct.AnnualIntrRate = annualIntrRate;
            acct.Deposit(balance);

            //test the withdrawal method
            acct.Withdraw(500);


            //refresh the list of accounts displayed in the UI
            _lstAccountList.Items.Add(acct);
        }
    }
}
