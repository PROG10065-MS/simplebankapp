﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAppLogic
{
    //TODO: Define an SavingsAccount class as described by the UML diagram provided with the 
    //ATM Simulator Case Study. Derive the SavingsAccount class from Account
    class SavingsAccount : Account
    {
        public SavingsAccount(int acctNo, string acctHolderName) : base(acctNo, acctHolderName)
        {
        }

        public override void Deposit(double amount)
        {
            if (amount < 0)
            {
                Debug.Assert(false, "Cannot withdraw a negative amount");
                return;
            }

            _balance += amount*2; //*2 because of auto-deposit
        }

        public override void Withdraw(double amount)
        {
            if (amount < 0)
            {
                Debug.Assert(false, "Cannot withdraw a negative amount");
                return;
            }

            _balance -= amount; 
        }
    }
}
