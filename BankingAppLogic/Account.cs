﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAppLogic
{
    //TODO: Define an Account class as described by the UML diagram provided with the 
    //ATM Simulator Case Study
    public abstract class Account
    {
        private int _acctNo;

        private string _acctHolderName;

        protected double _balance;

        protected float _annualIntrRate;

        public Account(int acctNo, string acctHolderName)
        {
            _acctNo = acctNo;
            _acctHolderName = acctHolderName;
            _balance = 0;
            _annualIntrRate = 0.0f;
        }

        //TODO: Create properties for access to account field variables
        public int AccountNumber
        {
            get { return _acctNo; }
            set { _acctNo = value; }
        }

        public string AcctHolderName
        {
            get { return _acctHolderName; }
            set { _acctHolderName = value; }
        }

        public virtual float AnnualIntrRate
        {
            get { return _annualIntrRate; }
            set { _annualIntrRate = value; }
        }

        public float MonthlyIntrRate
        {
            get { return _annualIntrRate / 12; }
        }

        public double Balance
        {
            get { return _balance; }
        }

        public abstract void Deposit(double amount);

        public abstract void Withdraw(double amount);
        

        public override string ToString()
        {
            return $"{_acctNo}: Customer: {_acctHolderName}\tBalance: {_balance}$";
        }
    }
}
