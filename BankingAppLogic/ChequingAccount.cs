﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAppLogic
{
    //TODO: Define an ChequingAccount class as described by the UML diagram provided with the 
    //ATM Simulator Case Study. Derive the ChequingAccount class from Account
    class ChequingAccount : Account
    {
        private const double OVERDRAFT_LIMIT = 500.0;

        private const float MAX_INTEREST_RATE = 1.0F;

        public ChequingAccount(int acctNo, string acctHolderName)
            : base(acctNo, acctHolderName)
        {

        }

        public override void Deposit(double amount)
        {
            if (amount < 0)
            {
                Debug.Assert(false, "Cannot deposit a negative amount");
                return;
            }

            _balance += amount;
        }

        //TODO: override the Withdraw method to allow withdrawls within the overdraft limit
        public override void Withdraw(double amount)
        {
            if (amount < 0)
            {
                Debug.Assert(false, "Cannot withdraw a negative amount");
                return;
            }

            //check for the overdraft limit
            if (_balance + OVERDRAFT_LIMIT < amount)
            {
                Debug.Assert(false, "Insufficient funds.");
                return;
            }

            _balance -= amount;

        }
    }
}
