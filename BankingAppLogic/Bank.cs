﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAppLogic
{
    //TODO: Define an enum for type of account
    public enum AccountType
    {
        CHECQUING_ACCOUNT = 1,
        SAVINGS_ACCOUNT
    }

    //TODO: Define the Bank class that manages a list of accounts and provides
    //access to those accounts
    public class Bank
    {
        private List<Account> _accountList;

        public Bank()
        {
            _accountList = new List<Account>();
        }

        //TODO: Define a read-only properties to allow clients to iterate through
        //the iist of accounts
        public IEnumerable<Account> Accounts
        {
            get { return _accountList; }
        }

        //TODO: Define a method called "OpenAccount" that will taken in the client name, account type and account number
        //and create the appropriate instance of account (savings or chequing), set its properties and add it to the 
        //the account list. Have the method return the newly created account
        public Account OpenAccount(string clientName, AccountType acctType, int acctNo)
        {
            //depending on account type create a savings account or a checking account
            Account newAccount;
            switch (acctType)
            {
                case AccountType.CHECQUING_ACCOUNT:
                    newAccount = new ChequingAccount(acctNo, clientName);
                    break;

                case AccountType.SAVINGS_ACCOUNT:
                    newAccount = new SavingsAccount(acctNo, clientName);
                    break;

                default:
                    Debug.Assert(false, "Incorrect account type. Cannot create account");
                    return null;
            }

            //add the acccount to the list
            _accountList.Add(newAccount);
            return newAccount;
        }

    }
}
